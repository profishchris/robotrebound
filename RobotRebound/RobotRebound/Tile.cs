﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Holds information about the tile used to create the platforms.
   inherits from GameObject. Very straight forward; logic surrounding it's use resides in other classes  */

namespace RobotRebound
{
    enum TileType
    {
        Blank = 0,
        Enemy,
        Platform,
        Flames,
        Door,
        Pickup
    }

    class Tile : GameObject
    {
        // Use consts for these, so that the grid can be set-up before the textures are loaded
        const int mWidth = 40;
        const int mHeight = 32;

        TileType mTileType;

        // Constructor
        public Tile()
        {
            Texture = null;
            mTileType = TileType.Blank;
            Posistion = Vector2.Zero;

        } 

        public Tile(Texture2D texture, TileType tileType, Vector2 posistion)
        {
            Texture = texture;
            mTileType = tileType;
            Posistion = posistion;
        }


        // Properties
        public static int Height
        {
            get { return mHeight; }
        }

        public static int Width
        {
            get { return mWidth; }
        }

        public TileType TileType
        {
            get { return mTileType; }
        }
    }
}
