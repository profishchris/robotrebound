﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* ExpertPlayer = Player + guns 
 * mostly uses overrides to add custom behaviour */

namespace RobotRebound
{

    class ExpertPlayer : Player
    {
        List<Bullet> mBulletList;
        double mFireDelay;
        const double mShotDelay = 0.4d;
        bool mCanFire = false;
        bool mHasShotBonus = false;
        const int mMaxShotBonus = 2;
        int mBonusShotsLeft;
        Texture2D mBulletTexture;

        ContentManager mPlayerContent;              

        // SFX
        SoundEffect mSoundFire;
        SoundEffect mSoundEnemyDeath;


        public ExpertPlayer(Texture2D thisTexture, Vector2 thisPosistion, Level level, ContentManager content, int width)
            : base(thisTexture, thisPosistion, level, content, width)
        {
            Health = Player.StartHP;
            Speed = 0;
            Level = level;
            mBulletList = new List<Bullet>();
            mPlayerContent = content;
            mFireDelay = 0;

            base.PlayerNumber = 2;

            mBulletTexture = mPlayerContent.Load<Texture2D>("Shot");
            mSoundFire = content.Load<SoundEffect>("Effects/Shot");
            mSoundEnemyDeath = content.Load<SoundEffect>("Effects/EnemyDead");

        }

        // Properties
        public static int MaxShotBonus
        {
            get { return mMaxShotBonus; }
        }

        public int BonusShotsLeft
        {
            get { return mBonusShotsLeft; }
            set { mBonusShotsLeft = value; }

        }

        public bool HasShotBonus
        {
            get { return mHasShotBonus; }
            set { mHasShotBonus = value; }
        }


        // Methods
        public Bullet Fire()
        {
            Vector2 bulletPosistion = new Vector2(Bounds.Center.X, Posistion.Y + 30);
            Bullet newBullet = new Bullet(mBulletTexture, bulletPosistion, Facing);

            return newBullet;
        }

        public override bool UpdatePlayer(GamePadState gamePad, Camera2D cam, Player otherPlayer, GameTime gameTime) 
        {
            bool complete = base.UpdatePlayer(gamePad, cam, otherPlayer, gameTime);
            GamePadState previousGamePad = gamePad;
            gamePad = GamePad.GetState(PlayerIndex.Two);
            KeyboardState keys = Keyboard.GetState();


            if (mBonusShotsLeft >= 1)
            {
                mColour = Color.OrangeRed;
                
            }
            else
            {
                HasShotBonus = false;
                mBonusShotsLeft = 0;
                mColour = Color.White;
            }

            if (mBonusShotsLeft >= 2)
            {
                mFireDelay = mShotDelay;
            }

            // shot timing
            if (HasShotBonus)
            {
                mFireDelay += gameTime.ElapsedGameTime.TotalSeconds;
                if (mFireDelay >= mShotDelay)
                {
                    mFireDelay = 0;
                    mCanFire = true;
                }
            }

            // shooting logic
            bool playerMayShoot = gamePad.IsButtonDown(Buttons.B)  &&
                                    mCanFire;
            if (playerMayShoot)
            {
                mBulletList.Add(Fire());
                mCanFire = false;
                mBonusShotsLeft--;
                mSoundFire.Play();
            }


            // update bullets then check for collisions or off screen
            // remove if neccesary
            Bullet b;
            for (int i = mBulletList.Count; i > 0; i--)
            {
                b = mBulletList[i - 1];
                b.UpdateBullet();

                foreach (NPC monster in Level.Enemies)
                {
                    if (monster.Bounds.Intersects(b.Bounds))
                    {
                        monster.Health--;
                        if (monster.Health <= 0)
                        {
                            mSoundEnemyDeath.Play();
                        }
                        mBulletList.Remove(b);
                    }
                }

                foreach (Tile t in Level.TileArray)
                {
                    if (t.TileType == TileType.Platform)
                    {
                        if (t.Bounds.Intersects(b.Bounds))
                        {
                            mBulletList.Remove(b);
                        }

                    }

                }

                if (b.Posistion.X > cam.Posistion.X + cam.ScreenWidth ||
                    b.Posistion.X < cam.Posistion.X)
                {
                    mBulletList.Remove(b);
                }

            }

            return complete;
        }

        // overide this so that we can add check for flame blocks
        protected override Tile FindRistriction(int block, TileType other)
        {
            return base.FindRistriction(block, TileType.Flames);
           
        }

        protected override void CheckForRebound(Player otherPlayer)
        {
            // This player cann't rebound
        }

        protected override CollisionType CheckTiles(Tile[,] playerTiles)
        {
            // First, check the base function...
            // If it is blank, then we need to check 2 extra places for collision (due to sprite size)
            // finally return the accumulated result
            
            CollisionType theCollision = CollisionType.None;

            theCollision = base.CheckTiles(playerTiles);

            if (theCollision == CollisionType.None)
            {
                // if left collision
                if (playerTiles[0, 0].TileType == TileType.Platform)
                {
                    theCollision = CollisionType.Left;
                }


                //if right collision
                if (playerTiles[2, 0].TileType == TileType.Platform)
                {
                    theCollision =  CollisionType.Right;
                }
            }

            return theCollision;
        }

        public override void DrawPlayer(SpriteBatch batch)
        {

            base.DrawPlayer(batch);

            foreach (Bullet b in mBulletList)
            {
                batch.Draw(b.Texture, b.Posistion, Color.Red);
            }
        }



    }
}
