﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


/* This is the base class from which all other classes will inherit
 * it contains general data (texture posistion etc) for almost anything
 * that can be drawn to the screen */

namespace RobotRebound
{
    class GameObject
    {
        Texture2D mTexture;
        Vector2 mPosistion;
        Rectangle mBounds;

        // Constructors
        public GameObject()
        {
            mTexture = null;
            mPosistion = Vector2.Zero;
            mBounds = Bounds;
        }

        public GameObject(Texture2D texture, Vector2 posistion)
        {
            mTexture = texture;
            mPosistion = posistion;
            mBounds = Bounds;
        }

        // Properties
        public Texture2D Texture
        {
            get { return mTexture; }
            set { mTexture = value; }
        }

        public Vector2 Posistion
        {
            get { return mPosistion; }
            set { mPosistion = value; }
        }

        public virtual Rectangle Bounds
        {
            get
            {
                if (Texture != null)
                {
                    int x = (int)Posistion.X;
                    int y = (int)Posistion.Y;

                    return new Rectangle(x, y, Texture.Width, Texture.Height);
                }
                else
                {
                    return new Rectangle();
                }
            }
        }

    }
}

