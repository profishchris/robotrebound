﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* This class is designed for the novice player,
 * the expert player will inherit from this */

namespace RobotRebound
{
    enum Direction
    {
        Left = 0,
        Right
    }

    enum Mode
    {
        Normal = 0,
        Recovery
    }

    enum CollisionType
    {
        None = 0,
        Left,
        Right,
        Door,
        Fire
    }

    class Player : GameObject
    {
        int mHealth;
        float mRecoveryTime;
        Direction mDirection;

        Vector2 mPreviousPosistion;

        float mSpeed;
        Vector2 mHorizontalSpeed = new Vector2(3, 0);
        Vector2 mVerticalSpeed = new Vector2(0, 5);
        float mJumpSpeed;
        Vector2 mSpeedBonusAmount = new Vector2(2, 0);
        Vector2 mSpeedBonus = Vector2.Zero;
        bool mHasSpeedBonus = false;
        const double mMaxBonusTime = 5000.0f;
        double mBonusTime;           
        
        protected Color mColour = Color.White;

        Tile mCeiling;
        Level mLevel;
        bool mHasBeenSquashed = false;
        Tile[,] playerTiles;
        Tile mFloor;
        bool mIsJumping;

        Mode mCurrentMode;
        const float mMaxRecoveryTime = 2.5f;

        bool mTextureOff;
        int mPlayerNumber = 1;


        // jumping consts
        const float mVelocity = 5f;
        const float mGravity = 0.5f;
        const float mInitalJumpSpeed = 10f;

        // Animation
        Animation mSprites;
        Rectangle mSource;
        double mAnimationTimer = 0;
        const double mAnimationDelay = 0.25d;
        Glow mGlow;

        // For debugging keys
        KeyboardState currentKeys = Keyboard.GetState();
        KeyboardState previousKeys;

        // SFX
        SoundEffect soundJump;
        SoundEffect soundRebound;
        SoundEffect soundPowerUpFire;
        SoundEffect soundPowerUpSpeed;

        // Score stuff
        double mScoreTime = 0d;

        // Debug pad
        GamePadState debugPad;
        GamePadState previousDebugPad;


        // this constructor is for NPCS to call
        public Player(Texture2D texture, Vector2 posistion, Level level, int width)
            : base(texture, posistion)
        {
            mHealth = Player.StartHP;
            mSpeed = 0;
            mIsJumping = false;
            mJumpSpeed = 0;
            mLevel = level;
            mDirection = Direction.Right;
            mTextureOff = false;
            mCurrentMode = Mode.Normal;
            mPreviousPosistion = posistion;

            mSprites = new Animation(Texture, width);
            mSource = mSprites.NextFrame();

        }

        // this Constructor is for players
        public Player(Texture2D texture, Vector2 posistion, Level level, ContentManager Content, int width)
            : base(texture, posistion)
        {
            mSpeed = 0;
            mIsJumping = false;
            mJumpSpeed = 0;
            mDirection = Direction.Right;
            mTextureOff = false;
            mCurrentMode = Mode.Normal;
            mPreviousPosistion = posistion;
            mLevel = level;

            mSprites = new Animation(Texture, width);
            mSource = mSprites.NextFrame();

            mGlow = new Glow(this, Content, level);

            soundJump = Content.Load<SoundEffect>("Effects/Jump");
            soundRebound = Content.Load<SoundEffect>("Effects/Rebound");
            soundPowerUpFire = Content.Load<SoundEffect>("Effects/PowerUpFire");
            soundPowerUpSpeed = Content.Load<SoundEffect>("Effects/PowerUpSpeed");

        }


        // Properties
        public bool HasBeenSquashed
        {
            get { return mHasBeenSquashed; }
            set { mHasBeenSquashed = value; }
        }

        public double GetScore
        {
            get
            {
                return mScoreTime;
            }
        }

        private double BonusTime
        {
            set { mBonusTime = value; }
        }

        public bool HasSpeedBonus
        {
            get { return mHasSpeedBonus; }
            set { mHasSpeedBonus = value; }

        }

        protected int PlayerNumber
        {
            get { return mPlayerNumber; }
            set { mPlayerNumber = value; }
        }

        private Rectangle Source
        {
            get { return mSource; }
            set { mSource = value; }
        }

        private Animation Sprites
        {
            get { return mSprites; }
            set { mSprites = value; }
        }

        public override Rectangle Bounds
        {
            get
            {
                if (mSprites != null)
                {
                    return new Rectangle((int)Posistion.X,
                                         (int)Posistion.Y,
                                         mSprites.SpriteWidth,
                                         mSprites.SpriteHeight);
                }

                return base.Bounds;
            }
        }

        public static int StartHP
        {
            get { return 3; }
        }

        protected Direction Direction
        {
            get { return mDirection; }
            set { mDirection = value; }
        }

        protected Level Level
        {
            set { mLevel = value; }
            get { return mLevel; }
        }

        public int Health
        {
            get { return mHealth; }
            set { mHealth = value; }
        }

        protected float Speed
        {
            get { return mSpeed; }
            set { mSpeed = value; }
        }

        private float JumpSpeed
        {
            get { return mJumpSpeed; }
            set { mJumpSpeed = value; }
        }

        protected Direction Facing
        {
            get { return mDirection; }
            set { mDirection = value; }
        }


        // Methods
        public void ResetScores()
        {
            this.mScoreTime = 0;
        }

        private void UpdateBonus(GameTime gameTime)
        {
            mBonusTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (mBonusTime >= mMaxBonusTime)
            {
                HasSpeedBonus = false;
                mBonusTime = 0.0d;
            }

            if (HasSpeedBonus)
            {
                mSpeedBonus = mSpeedBonusAmount;
                mColour = Color.MediumTurquoise;
            }
            else
            {
                mSpeedBonus = new Vector2(1, 0);
                mColour = Color.White;
            }

        }

        private void UpdateControls(GamePadState gamePad, Camera2D cam, GameTime gameTime, CollisionType collision, bool currentState)
        {
            previousKeys = currentKeys;
            currentKeys = Keyboard.GetState();
            // Grab previous state
            GamePadState previousGamePad = gamePad;
            previousDebugPad = debugPad;
            debugPad = GamePad.GetState(PlayerIndex.Three);


            // if we landed on fire and it's the novice player, kill the player
            if (collision == CollisionType.Fire && PlayerNumber == 1)
            {
                Health = 0;
            }

            if (mSource == Rectangle.Empty ||
                mAnimationTimer >= mAnimationDelay)
            {
                mSource = mSprites.NextFrame();
                mAnimationTimer = 0;
            }

            currentState = (currentKeys.IsKeyDown(Keys.D) || gamePad.DPad.Right == ButtonState.Pressed) &&
                            collision != CollisionType.Right;
            if (currentState)
            {
                Posistion += mHorizontalSpeed * mSpeedBonus;
                Facing = Direction.Right;
                mAnimationTimer += gameTime.ElapsedGameTime.TotalSeconds;
            }

            currentState = (currentKeys.IsKeyDown(Keys.A) || gamePad.DPad.Left == ButtonState.Pressed) &&
                            collision != CollisionType.Left;
            if (currentState)
            {
                Posistion -= mHorizontalSpeed * mSpeedBonus;
                Facing = Direction.Left;
                mAnimationTimer += gameTime.ElapsedGameTime.TotalSeconds;
            }

            currentState = currentKeys.IsKeyDown(Keys.Space) || gamePad.IsButtonDown(Buttons.A);
            if (currentState &&
                !mIsJumping)
            {
                soundJump.Play();
                mIsJumping = true;
                JumpSpeed = mInitalJumpSpeed;
                Posistion -= new Vector2(0, 5);
            }

            

            #region Debug Keys
            currentState = debugPad.IsButtonDown(Buttons.RightShoulder) &&
                           previousDebugPad.IsButtonUp(Buttons.RightShoulder);
            if (currentState)
            {
                mCurrentMode = Mode.Recovery;
                mRecoveryTime = 0;
            }


            currentState = debugPad.IsButtonDown(Buttons.LeftShoulder) &&
                           previousDebugPad.IsButtonUp(Buttons.LeftShoulder);
            if (currentState)
            {
                Level.NovicePlayer.Health--;

            }

            currentState = debugPad.IsButtonDown(Buttons.X) &&
                           previousDebugPad.IsButtonUp(Buttons.X);
            if (currentState)
            {
                Level.NovicePlayer.BonusTime = 0.0d;
                Level.NovicePlayer.HasSpeedBonus = true;
                soundPowerUpSpeed.Play();
            }

            currentState = debugPad.IsButtonDown(Buttons.B) &&
                           previousDebugPad.IsButtonUp(Buttons.B);
            if (currentState)
            {
                Level.VeteranPlayer.HasShotBonus = true;
                Level.VeteranPlayer.BonusShotsLeft = ExpertPlayer.MaxShotBonus;
                soundPowerUpFire.Play();
            }
            #endregion
        }

        public virtual bool UpdatePlayer(GamePadState gamePad, Camera2D cam, Player otherPlayer, GameTime gameTime)
        {
            CollisionType collision = CheckCollision(cam);

            // check to see if we hit the exit
            if (collision == CollisionType.Door)
            {
                return true;
            }
             
            bool currentState;

            // Debug Key
            currentState = debugPad.IsButtonDown(Buttons.A) &&
                           previousDebugPad.IsButtonUp(Buttons.A);
            if (currentState)
            {
                return true;
            }

            UpdateControls(gamePad, cam, gameTime, collision, currentState);

            mHasBeenSquashed = false;

            mScoreTime += gameTime.ElapsedGameTime.TotalSeconds;

            UpdateBonus(gameTime);


            mFloor = FindRistriction(Bounds.Bottom / Tile.Height, TileType.Platform);
            mCeiling = FindRistriction(Bounds.Top / Tile.Height, TileType.Platform);

            if (mCeiling.TileType == TileType.Platform)
            {
                
                JumpSpeed = 0;
                Posistion += new Vector2(0, 5);
            }


            if (mIsJumping)
            {
                PerformJump(mFloor);

                if (this.PlayerNumber == 1)
                {
                    CheckForRebound(otherPlayer);
                }

            }
            else
            {
                // if the player's floor is blank
                // then they're prob falling; switch back to jumpin mode
                // else, ensure they are posistioned correctly
                if (mFloor.TileType == TileType.Blank)
                {
                    mIsJumping = true;
                }
                else
                {
                    // make sure this isn't done if we are jumping UPWARD
                    if (mPreviousPosistion.Y <= Posistion.Y)
                    {
                        Posistion = new Vector2(Posistion.X, mFloor.Posistion.Y - mSprites.SpriteHeight);// * 2);
                    }

                }

            }

            // check player pos angainst the camera pos, adjust if necessary
            // set a flag so we can 'squash' the player if they're also being stopped on the RHS
            if (this.Posistion.X <= cam.Posistion.X + 10)
            {
                this.Posistion = new Vector2(cam.Posistion.X + 10, this.Posistion.Y);

                // If we are being pushed AND their is right collsion
                // then we are being squashed
                // kill player
                if (collision == CollisionType.Right)
                {
                    HasBeenSquashed = true;

                }
            }


            // Either check for enemy collision or continue/ initiate recovery
            switch (mCurrentMode)
            {
                case Mode.Normal:
                    CheckEnemyCollision();
                    break;
                case Mode.Recovery:
                    RecoveryMode(gameTime);
                    break;
            }

            // update previous posistion
            mPreviousPosistion = Posistion;

            return false;
        }

        /// <summary>
        /// This actually deals with both enemy collision and powerups
        /// </summary>
        private void CheckEnemyCollision()
        {
            // Check for enemy collision
            foreach (NPC e in mLevel.Enemies)
            {
                if (e.Bounds.Intersects(this.Bounds))
                {
                    Level.NovicePlayer.Health--;
                    mRecoveryTime = 0;
                    mCurrentMode = Mode.Recovery;
                }
            }


            // Remove power ups as required, giving bonus to correct player.
            PowerUp p;
            for (int i = mLevel.PowerUps.Count; i > 0; i--)
            {
                p = mLevel.PowerUps[i - 1];
                if (p.Bounds.Intersects(this.Bounds))
                {
                    if (p.Bonus == PowerType.FirePower)
                    {
                        Level.VeteranPlayer.HasShotBonus = true;
                        Level.VeteranPlayer.BonusShotsLeft = ExpertPlayer.MaxShotBonus;
                        soundPowerUpFire.Play();
                    }
                    else
                    {
                        Level.NovicePlayer.BonusTime = 0.0d;
                        Level.NovicePlayer.HasSpeedBonus = true;
                        soundPowerUpSpeed.Play();
                    }

                    mLevel.PowerUps.Remove(p);
                }

            }

        }

        private CollisionType CheckCollision(Camera2D cam)
        {

            // Check for right/left collision by checking surrounding tiles
            // populate an array based on our posistion
            playerTiles = new Tile[3, 3];
            int centerX = (int)Math.Floor((float)Bounds.Center.X / Tile.Width);
            int centerY = (int)Math.Floor((float)Bounds.Center.Y / Tile.Height);

            if (centerX - 1 >= 0 && centerY - 1 >= 0)
            {
                for (int i = centerX - 1; i <= (int)centerX + 1; i++)
                {
                    for (int j = (int)centerY - 1; j <= centerY + 1; j++)
                    {
                        playerTiles[i - (int)centerX + 1, j - centerY + 1] = mLevel.TileArray[i, j];
                    }

                }
            }

            try
            {

                return CheckTiles(playerTiles);
            }
            catch
            {
                return CollisionType.None;
            }

        }

        protected virtual CollisionType CheckTiles(Tile[,] playerTiles)
        {
            // a platform at [0,1] or [0,2] is on the left
            // a platform at [2,1] or [2,2] is on the right
            // if stood on door
            if (playerTiles[1, 1].TileType == TileType.Door)
            {
                return CollisionType.Door;
            }

            if (playerTiles[1, 2].TileType == TileType.Flames &&
                PlayerNumber == 1)
            {
                return CollisionType.Fire;
            }

            // if left collision
            if (playerTiles[0, 1].TileType == TileType.Platform ||
                playerTiles[0, 2].TileType == TileType.Platform)
            {
                return CollisionType.Left;
            }


            //if right collision
            if (playerTiles[2, 1].TileType == TileType.Platform ||
                playerTiles[2, 2].TileType == TileType.Platform)
            {
                return CollisionType.Right;
            }

            return CollisionType.None;
        }

        protected virtual void CheckForRebound(Player otherPlayer)
        {
            // if we intersect with the top of the other player,
            // make sure we're 'jumping'
            // wack jump speed up

            if (otherPlayer.Bounds.Intersects(this.Bounds))
            {
                this.mIsJumping = true;
                JumpSpeed = mInitalJumpSpeed * 2;
                soundRebound.Play();
            }


        }

        private void RecoveryMode(GameTime gameTime)
        {
            mTextureOff = !mTextureOff;

            mRecoveryTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (mRecoveryTime >= mMaxRecoveryTime)
            {
                mCurrentMode = Mode.Normal;
                mTextureOff = false;
            }

        }        

        private bool IsTouchingFloor()
        {
            int bottom = Bounds.Bottom / Tile.Height;
            int center = (int)Math.Floor((float)Bounds.Center.X / Tile.Width);

            for (int i = center - 1; i <= center + 1; i++)
            {
                if (mLevel.TileArray[i, bottom].TileType == TileType.Platform ||
                    mLevel.TileArray[i, bottom].TileType == TileType.Flames)
                {
                    return true;
                }
            }

            return false;
        }

        // 2params are needed here as player 2 needs to check a differnet type
        protected  virtual Tile FindRistriction(int block, TileType other)
        {
            // block represents either the top or bottom tile relative to the array
            // calculate the center
            int center = (int)Math.Floor((double)Bounds.Center.X / Tile.Width);
            for (int i = center - 1; i <= center + 1; i++)
            {
                // if the tile is a platform AND it's within a quarter tile (also flames for player 2)
                // return the tile. Otherwise return blank.
                Tile theTile = mLevel.TileArray[i, block];
                if ((mLevel.TileArray[i, block].TileType == other ||
                    mLevel.TileArray[i, block].TileType == TileType.Platform) &&
                    Math.Abs(mLevel.TileArray[i, block].Bounds.Center.X - this.Bounds.Center.X) < Tile.Width - Tile.Width / 4)
                {
                    return theTile;
                }
            }

            return new Tile();

        }

        private void PerformJump(Tile floor)
        {
            if (floor.TileType == TileType.Platform || floor.TileType == TileType.Flames)
            {
                mIsJumping = false;
                JumpSpeed = 0;
                //FindFloor(TileType.Platform);
            }
            else
            {
                Posistion -= new Vector2(0, JumpSpeed);
                JumpSpeed -= mGravity;
                JumpSpeed = MathHelper.Clamp(JumpSpeed, -20f, 20f);
            }

        }

        private bool WillCollide(Tile theTile)
        {
            if (theTile.TileType != TileType.Blank)
            {
                return true;
            }

            return false;
        }

        public virtual void DrawPlayer(SpriteBatch batch)
        {           

            SpriteEffects spriteEffect = SpriteEffects.None;
            if (mTextureOff)
            {
                mColour = Color.Transparent;
            }

            if (Direction != RobotRebound.Direction.Left)
            {
                spriteEffect = SpriteEffects.FlipHorizontally;
            }

            Vector2 origin = new Vector2(0, 0);

            if (mSprites != null)
            {
                batch.Draw(Texture, Posistion, mSource, mColour, 0.0f, origin, 1.0f, spriteEffect, 0);
            }

            if (mGlow != null)
            {
                mGlow.DrawHealth(batch);
            }

            //if (mFloor.Texture != null)
            //{
            //    //    if (mFloor.Texture != null)
            //    //    {
            //    //        batch.Draw(mFloor.Texture, mFloor.Posistion, null,  Color.Green, 0, Vector2.Zero, 2f, SpriteEffects.None, 0);
            //    //    }


            //    batch.Draw(mFloor.Texture, new Vector2(Bounds.Center.X, Bounds.Center.Y), Color.Red);

            //}
        }

    }
}
