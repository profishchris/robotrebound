﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Holds limited info about power-ups */
namespace RobotRebound
{
    enum PowerType
    {
        SpeedPower = 0,
        FirePower
    }

    class PowerUp : GameObject
    {
        PowerType mBonus;


        public PowerUp(Texture2D thisTexture, Vector2 thisVector, PowerType bonus)
            : base(thisTexture, thisVector)
        {
            mBonus = bonus;
        }


        //Properties
        public PowerType Bonus
        {
            get { return mBonus; }
        }


        //Methods
        public void DrawPowerUp(SpriteBatch batch)
        {
            batch.Draw(Texture, Posistion, Color.White);

        }
    }
}
