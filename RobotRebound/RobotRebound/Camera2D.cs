﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* this class is in charge of the 2D camera used to scroll the game world
 * simply adding the GetTransform Matrix to our SpriteBatch.Begin will implement this.
 * */

namespace RobotRebound
{
    class Camera2D : GameObject
    {
        const float cameraSpeed = 0.075f;
        float mScreenWidth;  


        public Camera2D()
        {
            this.Posistion = Vector2.Zero;
        }


        public float ScreenWidth
        {
            get { return mScreenWidth; }
        }

        // Declare a public constent that sets the camera speed
        public static float CameraSpeed
        {
            get { return cameraSpeed; }
        }

        // Was used to test the class. Might be useful again
        public void Move(Vector2 move)
        {
            Posistion += move;
        }

        public Matrix GetTransform(GraphicsDevice device)
        {
            mScreenWidth = device.Viewport.Width;
            // Could potentialy add rotation and zoom here at a later date
            return Matrix.CreateTranslation(new Vector3(-Posistion.X, -Posistion.Y, 0));
        }

    }
}
