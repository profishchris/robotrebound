﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;

/* simple struct to hold info about Menu items */
namespace RobotRebound
{
    public struct Menu
    {
        public string message;
        public PlayMode mode;

        public Menu(String msg, PlayMode play)
        {
            message = msg;
            mode = play;
        }
    }
}
