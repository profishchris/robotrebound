﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Class for handling bullets*/

namespace RobotRebound
{
    class Bullet : GameObject
    {
        Direction mDirection;
        bool mIsDead;

        Vector2 mFireSpeed = new Vector2(15, 0);


        public Bullet(Texture2D thisTexture, Vector2 thisPosistion, Direction thisDirection)
            : base(thisTexture, thisPosistion)
        {
            mDirection = thisDirection;
            mIsDead = false;
        }



        public bool IsDead
        {
            get { return mIsDead; }
            set { mIsDead = value; }
        }

        public void UpdateBullet()
        {
            if (this.mDirection == Direction.Right)
            {
                Posistion += mFireSpeed;
            }
            else
            {
                Posistion -= mFireSpeed;
            }

        }


    }
}
