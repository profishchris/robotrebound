﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* class to handle spritesheets */

namespace RobotRebound
{
    class Animation
    {
        Vector2 mSpritePosistion = Vector2.Zero;
        Texture2D mSpriteTexture;
        Rectangle mSource;

        int mSpriteWidth;
        int mSpriteHeight;

        int mNumberOfFrames;
        int mCurrentFrameNumber;


        public Animation(Texture2D texture, int width)
        {
            mSpriteTexture = texture;
            mSpriteWidth = width;

            mSpriteHeight = texture.Height;

            mSource = new Rectangle(0, 0,
                                    mSpriteWidth,
                                    mSpriteHeight);

            mNumberOfFrames = NumberOfFrames();
            mCurrentFrameNumber = 0;

        }

        public int SpriteWidth
        {
            get { return mSpriteWidth; }
        }

        public int SpriteHeight
        {
            get { return mSpriteHeight; }
        }

        private int NumberOfFrames()
        {
            if (mSpriteTexture.Width % mSpriteWidth == 0)
            {
                return mNumberOfFrames = mSpriteTexture.Width / mSpriteWidth;
            }
            else
            {
                // halt the game if something is wrong with the sprite width
                throw new BadImageFormatException("Incorrect sprite width");
            }

        }

        public Rectangle NextFrame()
        {
            if (mCurrentFrameNumber + 1 < mNumberOfFrames)
            {
                mCurrentFrameNumber++;

            }
            else
            {
                mCurrentFrameNumber = 0;
            }

            mSource = new Rectangle(mSpriteWidth * mCurrentFrameNumber,
                                        mSource.Y,
                                        mSource.Width,
                                        mSource.Height);

            return mSource;
        }



    }
}
