﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;

/* Responsible for loading the level from a text file.
 * Lines are read in, stored in a list, then parsed;
 * then tiles are loaded besed on the char found
 * 
 * For a complete screen, the text file needs to be;
 *  1280 / 40 = 32 chars wide;
 *  720 / 32 = 22.5 chars heigh (ie 23, with the bottom row cut in half) */

namespace RobotRebound
{
    class Level : Game1
    {
        Tile[,] mTiles;
        int mGridWidth;
        int mGridHeight;
        bool mIsScrollingLevel;

        //used to hold player number
        public Player mNovicePlayer;
        ExpertPlayer mExpertPlayer;

        List<NPC> mEnemies;
        List<PowerUp> mBonuses;
        List<Tile> mPlatformList;

        ContentManager mLevelContent;

        // Textures
        Texture2D mPlatform;
        Texture2D mFire;
        Texture2D mBullet;
        Texture2D mPlayer1;
        Texture2D mPlayer2;
        Texture2D mExit;
        Texture2D mRedPower;
        Texture2D mBluePower;
        Texture2D mMonster;

        public Level(ContentManager content, bool scrollingLevel)
        {
            mLevelContent = content;
            mBonuses = new List<PowerUp>();
            mPlatformList = new List<Tile>();
            mEnemies = new List<NPC>();
            mIsScrollingLevel = scrollingLevel;

            mPlatform = mLevelContent.Load<Texture2D>("Tiles/Tile");
            mFire = mLevelContent.Load<Texture2D>("Tiles/Fire");
            mBullet = mLevelContent.Load<Texture2D>("Shot");
            mPlayer1 = mLevelContent.Load<Texture2D>("Units/Player1");
            mPlayer2 = mLevelContent.Load<Texture2D>("Units/Player2");
            mExit = mLevelContent.Load<Texture2D>("Tiles/Door");
            mRedPower = mLevelContent.Load<Texture2D>("PowerUps/RedPower");
            mBluePower = mLevelContent.Load<Texture2D>("PowerUps/BluePower");
            mMonster = mLevelContent.Load<Texture2D>("units/Monster");

        }


        // Properties
        public Player NovicePlayer
        {
            get { return mNovicePlayer; }

        }

        public ExpertPlayer VeteranPlayer
        {
            get { return mExpertPlayer; }
        }

        public bool IsScrollingLevel
        {
            get { return mIsScrollingLevel; }
        }

        public List<NPC> Enemies
        {
            get { return mEnemies; }
        }

        private int GridWidth
        {
            get { return mGridWidth; }
        }

        private int GridHeight
        {
            get { return mGridHeight; }
        }

        public Tile[,] TileArray
        {
            get { return mTiles; }
        }

        public List<Tile> Platforms
        {
            get { return mPlatformList; }
        }

        public List<PowerUp> PowerUps
        {
            get { return mBonuses; }

        }


        // Methods
        public void LoadLevel(string levelPath)
        {
            // list to store level segment info
            List<string> segment = new List<string>();

            // open file
            using (StreamReader fileIn = new StreamReader(levelPath))
            {
                // read line
                string lineIn = fileIn.ReadLine();
                mGridWidth = lineIn.Length;

                // keep reading lines till EoF
                while (lineIn != null)
                {
                    segment.Add(lineIn);
                    lineIn = fileIn.ReadLine();
                }

            }

            // Load the grid
            mGridHeight = segment.Count;
            mTiles = new Tile[mGridWidth, mGridHeight];
            for (int i = 0; i < mGridWidth; i++)
            {
                for (int j = 0; j < mGridHeight; j++)
                {
                    /* the list access is "backwards" because;
                     * we are acessing the 'i'th char (which translates to width)
                     * of the 'j'th string (which translates to height) */
                    char tileRepresentation = segment[j][i];
                    mTiles[i, j] = LoadTile(tileRepresentation, i, j);

                }
            }

        }

        private Tile LoadTile(char tileType, int x, int y)
        {
            /* Legend;
             * . = air,
             * m = mob,
             * p = platform,
             * n = novice start posistion (top half),
             * e = expert start posistion (bottom half),
             * b = speed power up (appears in btm half, powers the novice)
             * r = fire power up (appears in top half, powers the expert) 
             * f = fire platform
             * d = exit tile */
            switch (tileType)
            {
                case 'A':
                case 'a':
                case '.':
                    return LoadPlatform(x, y, TileType.Blank, null);

                case 'M':
                case 'm':
                    return LoadEnemy(x * Tile.Width, y * Tile.Height - Tile.Height, mMonster);

                case 'P':
                case 'p':
                    return LoadPlatform(x, y, TileType.Platform, mPlatform);                   

                case 'N':
                case 'n':
                    return LoadPlayerStart(x, y, mPlayer1, tileType);

                case 'E':
                case 'e':
                    return LoadPlayerStart(x, y, mPlayer2, tileType);

                case 'R':
                case 'r':
                    return LoadPowerUp(x, y, mRedPower, PowerType.FirePower);

                case 'B':
                case 'b':
                    return LoadPowerUp(x, y, mBluePower, PowerType.SpeedPower);

                case 'F':
                case 'f':
                    return LoadPlatform(x, y, TileType.Flames, mFire);

                case 'D':
                case 'd':
                    return LoadPlatform(x, y, TileType.Door, mExit);

                default:
                    throw new NotSupportedException("Unsupported tile");

            }

        }

        private Tile LoadPlatform(int x, int y, TileType tileType, Texture2D texture)
        {

            Tile newTile = new Tile(texture, tileType, new Vector2(x * Tile.Width, y * Tile.Height));
            mPlatformList.Add(newTile);
            return newTile;

        }

        private Tile LoadEnemy(int x, int y, Texture2D texture)
        {
            NPC enemy = new NPC(texture, new Vector2(x, y), this, 40);

            mEnemies.Add(enemy);

            return new Tile(null, TileType.Blank, new Vector2(x * Tile.Width, y * Tile.Height));
        }

        private Tile LoadPowerUp(int x, int y, Texture2D texture, PowerType type)
        {
            float startX = x * Tile.Width;
            float startY = y * Tile.Height;

            PowerUp power = new PowerUp(texture, new Vector2(startX, startY), type);
            mBonuses.Add(power);

            return new Tile(null, TileType.Blank, new Vector2(x * Tile.Width, y * Tile.Height));

        }

        private Tile LoadPlayerStart(int x, int y, Texture2D texture, char tileType)
        {
            float startX = x * Tile.Width;
            float startY = y * Tile.Height - texture.Height;

            if ((tileType == 'n' || tileType == 'N') &&
                mNovicePlayer == null)
            {
                mNovicePlayer = new Player(texture, new Vector2(startX, startY), this, mLevelContent, 64);
            }
            else if ((tileType == 'e' || tileType == 'E') &&
                mExpertPlayer == null)
            {
                mExpertPlayer = new ExpertPlayer(texture, new Vector2(startX, startY), this, mLevelContent, 52);
            }
            else
            {
               throw new Exception("Incorrect Player Config");
            }

            return new Tile(null, TileType.Blank, new Vector2(x * Tile.Width, y * Tile.Height));
        }

        public bool UpdateLevel(Camera2D cam, GameTime gameTime)
        {
            GamePadState gamePad1 = GamePad.GetState(PlayerIndex.One);
            GamePadState gamePad2 = GamePad.GetState(PlayerIndex.Two);


            bool complete = mNovicePlayer.UpdatePlayer(gamePad1, cam, mExpertPlayer, gameTime);
            //bool complete = false;
            mExpertPlayer.UpdatePlayer(gamePad2, cam, mNovicePlayer, gameTime);

            // Update and remove enemies as required
            for (int i = mEnemies.Count; i > 0; i--)
            {
                if (mEnemies[i - 1].IsDead)
                {
                    mEnemies.Remove(mEnemies[i - 1]);
                }
                else
                {
                    mEnemies[i - 1].UpdateEnemy();
                }
            }

            return complete;
            
        }

        public void DrawLevel(SpriteBatch batch)
        {

            for (int i = 0; i < mGridWidth; i++)
            {
                for (int j = 0; j < mGridHeight; j++)
                {
                    // if texture is not null, ie there is a tile to draw
                    if (mTiles[i, j].Texture != null)
                    {
                        Vector2 pos = new Vector2(i * Tile.Width, j * Tile.Height);

                        batch.Draw(mTiles[i, j].Texture, pos, Color.White);

                    }
                }
            }


            if (mBonuses.Count > 0)
            {
                foreach (PowerUp p in mBonuses)
                {
                    p.DrawPowerUp(batch);
                }
            }

            foreach (NPC e in mEnemies)
            {

                e.DrawPlayer(batch);
            }

            mNovicePlayer.DrawPlayer(batch);
            mExpertPlayer.DrawPlayer(batch);

           

        }
    }
}
