﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace RobotRebound
{
    class Glow: GameObject
    {
        Texture2D mFullHealth;
        Texture2D mHalfHealth;

        Player mPlayer;
        Level mLevel;

        public Glow(Player player, ContentManager content, Level level)
        {
            base.Posistion = player.Posistion;
            mPlayer = player;
            mLevel = level;

            mFullHealth = content.Load<Texture2D>("Bubbles/glow");
            mHalfHealth = content.Load<Texture2D>("Bubbles/glow");
        }

        private Vector2 PlayerPosistion
        {
            get { return mPlayer.Posistion; }
        }

        public void DrawHealth(SpriteBatch batch)
        {

                Vector2 thisPosistion = new Vector2(mPlayer.Bounds.Center.X,
                                        mPlayer.Bounds.Center.Y);
                Rectangle sourceRectangle = new Rectangle(0, 0,
                                                          mFullHealth.Width,
                                                          mFullHealth.Height);
                Vector2 origin = new Vector2(mFullHealth.Width / 2, mFullHealth.Height / 2);


                if (mLevel.NovicePlayer.Health >= 3)
                {
                    batch.Draw(mFullHealth, thisPosistion, sourceRectangle, Color.Silver, 0, origin, 1.2f, SpriteEffects.None, 0);
                }
                else if (mLevel.NovicePlayer.Health == 2)
                {
                    batch.Draw(mHalfHealth, thisPosistion, sourceRectangle,  Color.Silver, 0, origin, 0.825f, SpriteEffects.None, 0);
                }

        }
    }
}
