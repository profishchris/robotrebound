﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* Should have used a Character base class, but tis too late to change; this class will inherit
 * from player and use overides to gain cutom behaviour */

namespace RobotRebound
{
    class NPC : Player
    {
        // we'll use this to plot the enemy movement
        Vector2 mStartPosistion;
        bool mIsDead = false;

        const int mMaxDistance = 75;
        const int mMovementSpeed = 2;

        public NPC(Texture2D texture, Vector2 posistion, Level level, int width)
            :base(texture, posistion, level, width)
        {
            base.Health = 1;
            base.Posistion = posistion;
            mStartPosistion = posistion;

            base.Direction = RobotRebound.Direction.Right;
            base.Speed = mMovementSpeed;
        }

        public bool IsDead
        {
            get { return mIsDead; }
        }

        private Vector2 StartPos
        {
            get { return mStartPosistion; }
            set { mStartPosistion = value; }
        }

        public void UpdateEnemy()
        {
            // Don't call base; we want custom behaviour 

            // Move right until we reach a max distance, then reverse direction, repeat
            if (Direction == RobotRebound.Direction.Right)
            {
                Posistion = new Vector2(Posistion.X + Speed, Posistion.Y);
                if (Math.Abs(StartPos.X - Posistion.X) >= mMaxDistance)
                {
                    Direction = RobotRebound.Direction.Left;
                }
            }
            else if (Direction == RobotRebound.Direction.Left)
            {
                Posistion = new Vector2(Posistion.X - Speed, Posistion.Y);
                if (Math.Abs(StartPos.X - Posistion.X) >= mMaxDistance)
                {
                    Direction = RobotRebound.Direction.Right;
                }
            }


            if (Health <= 0)
            {
                mIsDead = true;
            }
        }

        public void DrawEnemy(SpriteBatch batch)
        {

            batch.Draw(Texture, Posistion, Color.White);

        }

    }
}
