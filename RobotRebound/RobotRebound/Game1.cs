using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace RobotRebound
{

    public enum PlayMode
    {
        Menu,
        Playing,
        Transistion,
        LoadLevel,
        Fail,
        GameOver,
        Win,
        Credits,
        Instructions,
        Exit
    }

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Level mLevel;
        const int mDefaultLevelNumber = 0;
        int mLevelNumber = mDefaultLevelNumber;
        const int mNoOfLevels = 7;

        const int mMaxNoOfLives = 5;
        int mLives = mMaxNoOfLives;

        Camera2D camera;

        public bool mComplete = true;

        double mLastScore = 0;
        double mTotalScore = 0;

        GamePadState gamePad;
        GamePadState prevGamePad;

        PlayMode currentPlayMode = PlayMode.Menu;
        PlayMode previousPlayMode;

        // SFX
        SoundEffect mSoundComplete;
        SoundEffect mSoundDead;

        // Music
        SoundEffect mMusicMenu;
        SoundEffectInstance mMusicMenuInstance;
        SoundEffect mMusicBG;
        SoundEffectInstance mMusicBGInstance;        

        // Menu stuff
        int selectedIndex = 0;
        List<Menu> menuItems;
        Texture2D instructionsScreen;
        Texture2D failScreen;
        Texture2D gameOverScreen;
        Texture2D transistionScreen;
        Texture2D winScreen;
        Texture2D creditScreen;
        // the dancer
        Texture2D mDancerTexture;
        Animation mDancer;
        Rectangle mDancerSource;
        double mDanceTimer = 0;
        const double mDanceDelay = 0.25d;
        SpriteEffects dancerFlip = SpriteEffects.None;

        // Special text varibales
        SpriteFont text;
        SpriteFont menuFont;
        double flashText = 0;
        Random rand = new Random();
        int randNum; 

        //// FPS stuff
        //int frameRate = 0;
        //int frameCounter = 0;
        //TimeSpan timeElapsed = TimeSpan.Zero;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // single resolution target for 720p; xbox will scale automaticly this way
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;

        }

        protected override void Initialize()
        {
            Window.Title = "Robot Rebound";

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            text = Content.Load<SpriteFont>("Text");
            menuFont = Content.Load<SpriteFont>("Menu");
            instructionsScreen = Content.Load<Texture2D>("Menu/Instructions");
            failScreen = Content.Load<Texture2D>("Menu/Fail");
            gameOverScreen = Content.Load<Texture2D>("Menu/GameOver");
            transistionScreen = Content.Load<Texture2D>("Menu/Success");
            winScreen = Content.Load<Texture2D>("Menu/Win");
            creditScreen = Content.Load<Texture2D>("Menu/Credits");

            mDancerTexture = Content.Load<Texture2D>("Units/Player2");
            mDancer = new Animation(mDancerTexture, 52);

            camera = new Camera2D();

            previousPlayMode = currentPlayMode;

            mSoundDead = Content.Load<SoundEffect>("Effects/Dead");
            mSoundComplete = Content.Load<SoundEffect>("Effects/Complete");

            mMusicBG = Content.Load<SoundEffect>("Songs/BGMusic");
            mMusicBGInstance = mMusicBG.CreateInstance();
            mMusicBGInstance.IsLooped = true;
            mMusicMenu = Content.Load<SoundEffect>("Songs/MenuMusic");
            mMusicMenuInstance = mMusicMenu.CreateInstance();            
            mMusicMenuInstance.IsLooped = true;

            // Load menu
            menuItems = new List<RobotRebound.Menu>();
            menuItems.Add(new Menu("Play Game", PlayMode.Instructions));
            menuItems.Add(new Menu("Credits", PlayMode.Credits));
            menuItems.Add(new Menu("Exit", PlayMode.Exit));

        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }


        //////////
        // Methods
        protected override void Update(GameTime gameTime)
        {
            // allow us to return to menu at any time (so we can quit)
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                currentPlayMode = PlayMode.Menu;
            }

            switch (currentPlayMode)
            {
                case PlayMode.Menu:
                    UpdateMenu();
                    break;

                case PlayMode.Playing:
                    UpdatePlaying(gameTime);
                    break;

                    // where we go between levels
                case PlayMode.Transistion:
                    currentPlayMode = UpdateTransistion();
                    break;

                    // to ensure we only load a level once (otherwise things explode)
                    // also manages the music change from menu to level
                case PlayMode.LoadLevel:
                    CreateLevel();
                    break;

                case PlayMode.Fail:
                    UpdateFail();
                    break;

                case PlayMode.GameOver:
                    UpdateGameOver();
                    break;

                case PlayMode.Win:
                    UpdateWin();
                    break;

                case PlayMode.Exit:
                    this.Exit();
                    break;

                case PlayMode.Instructions:
                    currentPlayMode = GetPressedStart(PlayMode.LoadLevel);
                    if (currentPlayMode == PlayMode.LoadLevel)
                    {
                        mMusicMenuInstance.Stop();
                        mLevelNumber = mDefaultLevelNumber;
                        mTotalScore = 0;
                    }
                    break;
            }

            base.Update(gameTime);
        }

        private void CreateLevel()
        {
            if (mMusicMenuInstance.State == SoundState.Playing)
            {
                mMusicMenuInstance.Stop();
            }
            previousPlayMode = currentPlayMode;
            currentPlayMode = PlayMode.Playing;
            LoadLevel();
        }

        private void LoadLevel()
        {
            bool createScrolling = true;
            if (mLevelNumber <= 3)
            {
                createScrolling = false;
            }

            mLevel = new Level(Content, createScrolling);
            string levelString = string.Format("Content/Levels/{0}.txt", mLevelNumber.ToString());
            mLevel.LoadLevel(levelString);
            ResetPlayers();
            camera.Posistion = Vector2.Zero;
            mComplete = false;

        }

        private void ResetPlayers()
        {
            // Reset Novice
            mLevel.NovicePlayer.HasSpeedBonus = false;
            mLevel.NovicePlayer.Health = Player.StartHP;


            // Reset Expert
            mLevel.VeteranPlayer.HasShotBonus = false;
            mLevel.VeteranPlayer.Health = Player.StartHP;
        }

        private PlayMode GetPressedStart(PlayMode nextPlayMode)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed ||
                GamePad.GetState(PlayerIndex.Two).IsButtonDown(Buttons.Start))
            {
                return nextPlayMode;
            }

            return currentPlayMode;
        }

        private void UpdateLevel(Level level, GameTime gameTime)
        {
            //timeElapsed += gameTime.ElapsedGameTime;

            //if (timeElapsed > TimeSpan.FromSeconds(1))
            //{
            //    timeElapsed -= TimeSpan.FromSeconds(1);
            //    frameRate = frameCounter;
            //    frameCounter = 0;
            //}

            if (level.UpdateLevel(camera, gameTime))
            {
                mLevelNumber++;
                mComplete = true;
                mSoundComplete.Play();
            }

            if (level.IsScrollingLevel)
            {
                // Update the cameras X posistion by modifying it with the gametime
                float cameraX = 0;
                cameraX += Camera2D.CameraSpeed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                camera.Posistion += new Vector2(cameraX, camera.Posistion.Y);
            }


        }

        //////////////////////
        // GameState Functions
        private void UpdatePlaying(GameTime gameTime)
        {
            if (mMusicBGInstance.State != SoundState.Playing)
            {
                mMusicBGInstance.Volume = 1.0f;
                mMusicBGInstance.Play();
            }

            UpdateLevel(mLevel, gameTime);


            previousPlayMode = currentPlayMode;

            if (mLevel.NovicePlayer.Health <= 0 ||
                mLevel.NovicePlayer.HasBeenSquashed ||
                mLevel.VeteranPlayer.HasBeenSquashed)
            {

                currentPlayMode = PlayMode.Fail;
                mSoundDead.Play();
            }
            
            if (mComplete)
            {
                currentPlayMode = PlayMode.Transistion;
            }

        }

        private PlayMode UpdateTransistion()
        {
            if (previousPlayMode == PlayMode.Playing)
            {
                mLastScore = mLevel.NovicePlayer.GetScore;
                mTotalScore += mLevel.NovicePlayer.GetScore;
            }

            previousPlayMode = currentPlayMode;

            if (mLevelNumber <= mNoOfLevels)
            {
                return  GetPressedStart(PlayMode.LoadLevel);

            }
            else
            {
                // WIN                        
                return  GetPressedStart(PlayMode.Win);
            }

        }

        private void UpdateFail()
        {
            ResetPlayers();
            camera.Posistion = Vector2.Zero;

            if (previousPlayMode == PlayMode.Playing)
            {
                mLives--;
                mLevel.NovicePlayer.ResetScores();
                mLevel.VeteranPlayer.ResetScores();

                if (mLives < 0)
                {
                    currentPlayMode = PlayMode.GameOver;
                    randNum = rand.Next(0, 4);
                }

            }          
                       

            previousPlayMode = currentPlayMode;
            currentPlayMode = GetPressedStart(PlayMode.LoadLevel);
        }

        private void UpdateGameOver()
        {
            mMusicBGInstance.Stop();
            previousPlayMode = currentPlayMode;
            currentPlayMode = GetPressedStart(PlayMode.Menu);
            if (currentPlayMode == PlayMode.Menu)
            {
                mTotalScore = 0;
                mLives = mMaxNoOfLives;
                mLevelNumber = mDefaultLevelNumber;
                ResetPlayers();
            }
        }

        private void UpdateWin()
        {
            mMusicBGInstance.Stop();
            previousPlayMode = currentPlayMode;
        }

        private void UpdateMenu()
        {
            if (mMusicMenuInstance.State != SoundState.Playing)
            {
                mMusicBGInstance.Stop();
                mMusicMenuInstance.Volume = 0.5f;
                mMusicMenuInstance.Play();
            }

            previousPlayMode = currentPlayMode;
            currentPlayMode = MenuInput();

        }

        private PlayMode MenuInput()
        {
            prevGamePad = gamePad;
            gamePad = GamePad.GetState(PlayerIndex.One);

            bool allowMove = false;

            allowMove = gamePad.IsButtonDown(Buttons.DPadDown) &&
                        prevGamePad.IsButtonUp(Buttons.DPadDown) &&
                        selectedIndex < menuItems.Count;
            if (allowMove)
            {
                selectedIndex++;
                selectedIndex = (int)MathHelper.Clamp(selectedIndex, 0, menuItems.Count - 1);
            }

            allowMove = gamePad.IsButtonDown(Buttons.DPadUp) &&
                        prevGamePad.IsButtonUp(Buttons.DPadUp) &&
                        selectedIndex >= 0;
            if (allowMove)
            {
                selectedIndex--;
                selectedIndex = (int)MathHelper.Clamp(selectedIndex, 0, menuItems.Count - 1);
            }

            allowMove = (gamePad.IsButtonDown(Buttons.LeftShoulder) &&
                         prevGamePad.IsButtonUp(Buttons.LeftShoulder)) ||
                        (gamePad.IsButtonDown(Buttons.RightShoulder) &&
                         prevGamePad.IsButtonUp(Buttons.RightShoulder));
            if (allowMove)
            {
                if (dancerFlip == SpriteEffects.None)
                {
                    dancerFlip = SpriteEffects.FlipHorizontally;
                }
                else
                {
                    dancerFlip = SpriteEffects.None;
                }
            }
                           

            allowMove = gamePad.IsButtonDown(Buttons.A) &&
                        prevGamePad.IsButtonUp(Buttons.A);
            if (gamePad.IsButtonDown(Buttons.A))
            {
                flashText = 0;
                return menuItems[selectedIndex].mode;
            }

            return currentPlayMode;
        }        

        ///////////////
        // Draw methods
        private void DrawTransistion(SpriteBatch spriteBatch)
        {
            GraphicsDevice.Clear(Color.Black);

            string currentTime = String.Format("{0:0.00}", mLastScore);
            string totalTime = String.Format("{0:0.00}", mTotalScore);

            string[] levelComplete = { "Level Complete!" };
            string[] stats = { "Time: " + currentTime,
                                "Total Time: " + totalTime };

            spriteBatch.Begin();

            spriteBatch.Draw(transistionScreen, Vector2.Zero, Color.White);
            DrawText(levelComplete, menuFont, Color.White, graphics.GraphicsDevice.Viewport.Height / 5);
            DrawText(stats, text, Color.White, graphics.GraphicsDevice.Viewport.Height - graphics.GraphicsDevice.Viewport.Height / 5);


            spriteBatch.End();
        }

        private void DrawGameOver(SpriteBatch spriteBatch)
        {
            string[] randomInsult = {   "You Fail at life",
                                                "Your gaming career is OVER",
                                                "You call that jumping?",
                                                "The trick is to not lose",
                                                "Jumping: you're doing it wrong" };

            string[] topText = { "GAME OVER" };
            string[] insult = { randomInsult[randNum] };

            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            spriteBatch.Draw(gameOverScreen, Vector2.Zero, Color.White);
            DrawText(topText, text, Color.White, graphics.GraphicsDevice.Viewport.Height / 5);
            DrawText(insult, text, Color.White, graphics.GraphicsDevice.Viewport.Height - graphics.GraphicsDevice.Viewport.Height / 8);

            spriteBatch.End();
        }

        private void DrawFail(SpriteBatch spriteBatch)
        {
            GraphicsDevice.Clear(Color.Black);
            string[] stringTop = { "You FAILED!! Press Start to retry" };
            string[] stringLives = { mLives.ToString() + " Lives left" };


            spriteBatch.Begin();

            spriteBatch.Draw(failScreen, Vector2.Zero, Color.White);
            DrawText(stringTop, text, Color.White, graphics.GraphicsDevice.Viewport.Height / 5);
            DrawText(stringLives, text, Color.White, (graphics.GraphicsDevice.Viewport.Height - (graphics.GraphicsDevice.Viewport.Height / 8)));

            spriteBatch.End();
        }

        private void DrawPlaying(SpriteBatch spriteBatch)
        {
            GraphicsDevice.Clear(Color.Black);

            // Sets the Bare minimum requirment for the camera to be implemented
            spriteBatch.Begin(SpriteSortMode.BackToFront,
                                BlendState.AlphaBlend,
                                null,
                                null,
                                null,
                                null,
                                camera.GetTransform(GraphicsDevice));

            mLevel.DrawLevel(spriteBatch);

            spriteBatch.End();
        }

        private void DrawMenu(SpriteBatch spriteBatch, GameTime gameTime)
        {
            string titleString = "Robot Rebound";
            Vector2 stringSize = menuFont.MeasureString(titleString);
            Vector2 posistion = new Vector2((graphics.GraphicsDevice.Viewport.Width - stringSize.Length()) / 2,
                                            stringSize.Y);

            string[] menuStrings = new string[menuItems.Count];
            for (int i = 0; i < menuItems.Count; i++)
            {
                menuStrings[i] = menuItems[i].message;
            }  


            // Animate Dancer            
            mDanceTimer += gameTime.ElapsedGameTime.TotalSeconds;
            if (mDanceTimer >= mDanceDelay)
            {
                mDancerSource = mDancer.NextFrame();
                mDanceTimer = 0;                
            }

            Vector2 dancerPosistion = new Vector2((graphics.GraphicsDevice.Viewport.Width - mDancerTexture.Width / 4) / 2,
                                                  (graphics.GraphicsDevice.Viewport.Height - mDancerTexture.Height / 2) / 3);

                     
                   
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            spriteBatch.Draw(mDancerTexture, dancerPosistion, mDancerSource, Color.White, 0, Vector2.Zero, 1, dancerFlip, 0);

            spriteBatch.DrawString(menuFont, titleString, posistion, Color.White);
            
            DrawMenuText(menuStrings, text);
            
            spriteBatch.End();

        }

        private void DrawInstructions(SpriteBatch spriteBatch, GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            string startText = ("Press Start to begin!!");
            Vector2 textLength = menuFont.MeasureString(startText);
            Vector2 textPosistion = new Vector2((graphics.GraphicsDevice.Viewport.Width - (int)textLength.Length()) / 2,
                                                (graphics.GraphicsDevice.Viewport.Height - (graphics.GraphicsDevice.Viewport.Height / 4)));
            Color colour = Color.Black;
            flashText += gameTime.ElapsedGameTime.TotalSeconds;
            double flashTime = 0.7d;
            if (flashText < flashTime)
            {
                colour = Color.WhiteSmoke;
            }
            else if (flashText >= flashTime && flashText < flashTime * 2)
            {
                colour = Color.Black;
            }
            else
            {
                flashText = 0;
            }

            spriteBatch.Begin();

            spriteBatch.Draw(instructionsScreen, Vector2.Zero, Color.White);

            spriteBatch.DrawString(menuFont, startText, textPosistion, colour);

            spriteBatch.End();

        }

        private void DrawCredits(SpriteBatch spriteBatch)
        {

            graphics.GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            spriteBatch.Draw(creditScreen, Vector2.Zero, Color.White);

            spriteBatch.End();

        }

        private void DrawWin(SpriteBatch spriteBatch)
        {
            GraphicsDevice.Clear(Color.Black);

            string[] win = { "Congratulations!! You Completed the game :D" };
            string scoreToString = String.Format("{0:0.00}", mTotalScore);
            string[] score = { "Final Time: " + scoreToString,
                               "Press Back to return to menu "};

            spriteBatch.Begin();

            spriteBatch.Draw(winScreen, Vector2.Zero, Color.White);
            DrawText(win, text, Color.White, graphics.GraphicsDevice.Viewport.Height / 5);
            DrawText(score, text, Color.White, graphics.GraphicsDevice.Viewport.Height - graphics.GraphicsDevice.Viewport.Height / 12);

            spriteBatch.End();
        }

        protected override void Draw(GameTime gameTime)
        {

            switch (currentPlayMode)
            {
                case PlayMode.GameOver:
                    DrawGameOver(spriteBatch);
                    break;

                case PlayMode.Fail:
                    DrawFail(spriteBatch);
                    break;

                case PlayMode.Playing:
                    DrawPlaying(spriteBatch);
                    break;

                case PlayMode.Menu:
                    DrawMenu(spriteBatch, gameTime);
                    break;

                case PlayMode.Instructions:
                    DrawInstructions(spriteBatch, gameTime);
                    break;

                case PlayMode.Credits:
                    DrawCredits(spriteBatch);
                    break;

                case PlayMode.Transistion:
                    DrawTransistion(spriteBatch);
                    break;

                case PlayMode.Win:
                    DrawWin(spriteBatch);
                    break;

                    // default to a blank screen (is used with LoadLevel)
                default:
                    graphics.GraphicsDevice.Clear(Color.Black);
                    break;
            }



            base.Draw(gameTime);
        }

        // this allows us to provide optional params to the drawtext function
        private void DrawText(string[] line, SpriteFont font, Color color)
        {
            float verticalOffset = graphics.GraphicsDevice.Viewport.Height / 2;
            this.DrawText(line, font, color, verticalOffset);
        }

        private void DrawText(string[] line, SpriteFont font, Color color, float vOffset)
        {
            // Handles drawing centerd text by looping backwards through an array,
            // moving the text up with each iteration by the height of the string.
            // could be made a little more general and built into a class
            Vector2 stringHeight;
            Vector2 accumulatedHeight = Vector2.Zero;

            for (int i = line.Length - 1; i >= 0; i--)
            {
                stringHeight = font.MeasureString(line[i]);
                accumulatedHeight += stringHeight;

                spriteBatch.DrawString(font,
                    line[i],
                    new Vector2((graphics.GraphicsDevice.Viewport.Width - stringHeight.Length()) / 2,
                        ( vOffset - accumulatedHeight.Y)),
                    color);
            }

        }

        private void DrawMenuText(string[] line, SpriteFont font)
        {
            // same as DrawText, but with highlighting.
            Vector2 stringHeight;
            Vector2 accumulatedHeight = Vector2.Zero;

            Color colour;

            for (int i = line.Length - 1; i >= 0; i--)
            {
                stringHeight = font.MeasureString(line[i]);
                accumulatedHeight += stringHeight;

                colour = Color.White;
                if (i == selectedIndex)
                {
                    colour = Color.SlateBlue;
                }

                spriteBatch.DrawString(font,
                    line[i],
                    new Vector2((graphics.GraphicsDevice.Viewport.Width - stringHeight.Length()) / 2,
                        ((graphics.GraphicsDevice.Viewport.Height - graphics.GraphicsDevice.Viewport.Height / 4) - accumulatedHeight.Y)),
                    colour);
            }
        }

        //private void DrawDebug()
        //{
        //    frameCounter++;

        //    string mouseCords = string.Format("{0} {1}", mouse.X, mouse.Y);
        //    string fpsCounter = string.Format("FPS: {0}", frameRate);

        //    //spriteBatch.DrawString(text, mouseCords, new Vector2(10, 10), Color.White);
        //    // spriteBatch.DrawString(text, level.mNovicePlayer.Health.ToString(), new Vector2(10, 50), Color.White);
        //    spriteBatch.DrawString(text, mLevel.mNovicePlayer.Posistion.ToString(), new Vector2(250, 10), Color.White);
        //    spriteBatch.DrawString(text, fpsCounter, new Vector2(250, 40), Color.White);
        //    spriteBatch.DrawString(text, camera.Posistion.ToString(), new Vector2(250, 70), Color.White);
        //    spriteBatch.DrawString(text, mLevel.mNovicePlayer.mIsJumping.ToString(), new Vector2(10, 50), Color.White);
        //    spriteBatch.DrawString(text, mTotalScore.ToString(), new Vector2(10, 10), Color.White);

        //}
    }
}
